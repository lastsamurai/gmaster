#!/bin/bash
exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3
exec 1>/home/log.txt 2>&1
# output will go to the file '/home/log.txt':


hostnamectl set-hostname playlist-server 

apt-get update 

apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common 

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - 

add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

apt-get update

apt-get install -y docker-ce

apt-get update

apt-get install -y docker-compose

echo "ENVIROMENT IS FUULY SET UP!" > /home/DONE.txt

git clone https://gitlab.com/lastsamurai/playlistapi /opt/playlistapi/ &&
docker-compose -f /opt/playlistapi/docker-compose.yml up

