# gmaster

"Are you looking for an automated script to provision an Ubuntu Linux 18.04 server WITH YOUR ON STARTUP SCRIPT on Google Cloud Platform ?"
gmaster can help you SET EVERYTHING UP AUTOMATICALLY.

USAGE:

* gmaster init
* gmaster deploy [MACHINE-NAME] [STARTUP-SCRIPT]
* gmaster flush


# How to use gmaster

First, run:

```bash
gmaster init

the init command checks are required packages and then 
authenticates with your GCP.


```

IMPORTANT NOTE: make sure you enable your compute.instances.create permission on Google Cloud Platform.

as a sample startup script, you can find *startup.sh* in this repo.
*startup.sh* configures server hostname and sets up the enviroment required to run [PLAYLIST HTTP API](https://gitlab.com/lastsamurai/playlist) Service on the Ubuntu machine.
[PLAYLIST HTTP API](https://gitlab.com/lastsamurai/playlist) is a dockerized app that we run using docker-compose as the final command in *startup.sh*
Please refer to Playlist's gitlab repo for more information.

## ./gmaster init

The init command makes sure your gcloud SDK is up-to-date and ready-to-go.
It also opens up a link in the browser to log you in to your GCP.

p.s. you MUST first run *gmaster init* before you run either other command.

## ./gmaster deploy [MACHINE-NAME] [STARTUP-SCRIPT.SH]

the deploy command provisions an Ubuntu 18.04 server on the cloud with STARTUP-SCRIP.SH.  
it can take some time for the startup script to finish setting everything up. so once the machine is created, be patient and check out log.txt  
my sample startup.sh will write logs to /home/log.txt
therefore, after you "gcloud compute ssh root@MACHINE", make sure to check out:
```bash
tail -f /home/log.txt

```

## ./gmaster flush

the flush command deletes all your VMs on Google Cloud Platform.
while a dangerous command, it can help free up processnig power on the cloud.

